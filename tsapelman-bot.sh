#!/bin/sh

API_TOKEN=$1  # API TOKEN assigned to your bot by BotFather

BOTNAME="tsapelman-bot"
SLEEP_AFTER_FAIL=20

while true
do
    ./${BOTNAME}.py "${API_TOKEN}" >> ${BOTNAME}.log
    sleep $SLEEP_AFTER_FAIL
done
