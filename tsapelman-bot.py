#!/usr/bin/env python
# encoding: utf-8


import os
import urllib, urllib2
import json
import sys
import time
from datetime import datetime


DATA_DIR = '%s/.tsapelman-bot' % os.path.expanduser("~")
DATA_FILE = 'user-data.txt'
DATA_FILE_PATH = os.path.join(DATA_DIR, DATA_FILE)


# Returns current date and time as a string
def dateTimeStr():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')


# Parses user data string.
# Returns (chat_id, counter) tuple
def parseUserData(s):
    data = s.split(':')
    return (int(data[0]), int(data[1]))


# Parses the data file containing the data of all bot users.
# Returns dictionary of chat_id --> counter pairs.
def parseDataFile():
    try:
        with open(DATA_FILE_PATH) as f:
            return dict([parseUserData(x.strip()) for x in f.readlines() if x.strip()])
    except (OSError, IOError, IndexError) as e:
        return dict()


# Writes the data of all bot users into the data file
def refreshDataFile(user_data):
    try:
        if not os.path.exists(DATA_DIR):
            os.makedirs(DATA_DIR)
        with open(DATA_FILE_PATH, "w") as f:
            for key, value in user_data.iteritems():
                f.write("%d:%d\n" % (key, value))
    except (OSError, IOError) as e:
        pass


# Requests messages for the bot, parses them, refreshes user data and replies to appropriate user
def handleMessages(user_data):
    reply = json.loads(
            urllib2.urlopen('%s/getUpdates?%s' % (
                    REQUEST_STR, urllib.urlencode(handleMessages.data)
                    ), timeout = handleMessages.data['timeout'] * 2).read()
            )
    if not reply or not reply['ok'] or not ('result' in reply):
        print "Reply is not ok!"
        sys.stdout.flush()
        time.sleep(10)
        return
    offset = 0
    for item in reply['result']:
        offset = max(offset, item['update_id'])
        message = item['message']
        chat_id = int(message['chat']['id'])
        if chat_id not in user_data:
            user_data[chat_id] = -1
        user_data[chat_id] = user_data[chat_id] + 1
        sendMessage(chat_id, user_data[chat_id])
        refreshDataFile(user_data)

    handleMessages.data['offset'] = offset + 1

handleMessages.data = dict(offset = 0, timeout = 15, allowed_updates='["message"]')


# Sends a message with 'counter' to a user with 'chat_id' chat identifier
def sendMessage(chat_id, counter):
    data = dict()
    data['chat_id'] = chat_id
    data['text'] = 'You have sent me %d messages' % counter
    print "%s  Reply into chat %d: %s" % (dateTimeStr(), chat_id, counter)
    sys.stdout.flush()
    urllib.urlopen(
            '%s/sendMessage?%s' % (REQUEST_STR, urllib.urlencode(data))
            ).read()


# Execution start point
if len(sys.argv) != 2:
    sys.stderr.write("Pass me API_TOKEN as an argument and nothing more!\n")
    sys.exit(1)
API_TOKEN = str(sys.argv[1])
REQUEST_STR = "https://api.telegram.org/bot%s" % API_TOKEN

user_data = parseDataFile()
while True:
    try:
        handleMessages(user_data)
    except KeyboardInterrupt as e:
        sys.exit()
    except:
        pass
